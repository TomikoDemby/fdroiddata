Categories:Games
License:FreeBSD
Web Site:https://github.com/bobthekingofegypt/BobBall/blob/HEAD/README.mkd
Source Code:https://github.com/bobthekingofegypt
Issue Tracker:https://github.com/bobthekingofegypt/BobBall/issues

Auto Name:BobBall
Summary:Jezzball style clone
Description:
BobBall is a simple version of the old game Jezzball.

Features:

* Easy to play, hard to master
* Infinite levels
* Top scores table
.

Repo Type:git
Repo:https://github.com/bobthekingofegypt/BobBall.git

Build:1.0,1
    commit=v1.0

Build:1.3.1,5
    disable=resource issues
    commit=v1.3.1

Build:1.4,6
    disable=resource issues
    commit=v1.4

Build:1.5,7
    disable=resource issues
    commit=v1.5

Build:1.5.1,8
    commit=v1.5.1

Build:1.6,9
    commit=v1.6

Build:1.7,10
    commit=v1.7

Build:1.7.1,11
    commit=v1.7.1

Build:1.8,12
    commit=v1.8

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.8
Current Version Code:12

