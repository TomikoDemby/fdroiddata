Categories:System,Time
License:Apache2
Web Site:
Source Code:https://github.com/xsoh/Etar-Calendar
Issue Tracker:https://github.com/xsoh/Etar-Calendar/issues

Auto Name:Etar
Summary:Material designed calendar
Description:
Etar is material designed calendar based on the ASOP calendar.
.

Repo Type:git
Repo:https://github.com/xsoh/Etar-Calendar.git

Build:1.0,4
    disable=forces gradle 1.8, which is too old to run the build
    commit=v1.0
    submodules=yes
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:4

