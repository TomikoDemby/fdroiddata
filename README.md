# F-Droid Data

[![Build status](https://ci.gitlab.com/projects/5274/status.png?ref=master)](https://ci.gitlab.com/projects/5274?ref=master)

This repository holds general and build information for all the apps on our
main repo on f-droid.org.

## Quickstart

Clone [fdroidserver](https://gitlab.com/fdroid/fdroidserver):

	git clone https://gitlab.com/fdroid/fdroidserver.git

Add the cloned dir to your `PATH`:

	export PATH="$PATH:$PWD/fdroidserver

Enter it:

	cd fdroiddata

An empty configuration file should work as a start:

	touch config.py

Make sure fdroid works and reads the metadata files properly:

	fdroid readmeta

## Contributing

See the [Contributing](CONTRIBUTING.md) doc.

## More information

F-Droid is an app store application for androids. 
It is just like google play store but free of cost because it is an organizational app store and works cost free. 
You can install applications from F-Droid website or client app without registration for any account. 
It provides you source code of application and you can set your own app repository. These changes are made by http://onlineslotscanada.com/ and credit them whenever you make any changes in the application. If you want to install F-Droid client then you have to have to allow installation from unknown sources in android setting and have to execute APK from official site.


You can find more details on [the manual](https://f-droid.org/manual/).